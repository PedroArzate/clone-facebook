<?php

namespace App\Http\Controllers;

use App\Models\Groups;
use App\Http\Requests\GroupsRequest;

class GroupsController extends Controller
{
    /**
     * Display a listing of groups.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Groups::all();
    }

    /**
     * Store a newly created group in storage.
     *
     * @param  App\Http\Requests\GroupsRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(GroupsRequest $request)
    {
        $validated = $request->validated();

        $group = new Groups;
        $group->name         = $request->name;
        $group->description  = $request->description;
        $group->save();

        return $group;
    }

    /**
     * Display the specified group.
     *
     * @param  \App\Models\Groups  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $group = Groups::find($id);
        return $group;
    }

    /**
     * Update the specified group in storage.
     *
     * @param  App\Http\Requests\GroupsRequest  $request
     * @param  \App\Models\Groups  $id
     * @return \Illuminate\Http\Response
     */
    public function update(GroupsRequest $request, $id)
    {        
        $validated = $request->validated();

        $group = Groups::find($id);
        
        $group->name         = $request->name;
        $group->description  = $request->description;
        
        $group->update();

        return $group;
    }

    /**
     * Remove the specified group from storage.
     *
     * @param  \App\Models\Groups  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {        
        $group = Groups::find($id);

        if(is_null($group)) {
            return response()->json('The operation could not be performed successfully.', 404);
        }
        
        $group ->delete();
        return response()->noContent();
    }
}
