<?php

namespace App\Http\Controllers;

use App\Models\Posts;
use App\Models\Groups;
use App\Http\Requests\PostsRequest;

class PostsController extends Controller
{
    /**
     * Display a listing of posts.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Posts::join('groups', 'posts.group_id', '=', 'groups.id')
                    ->orderBy('is_fixed', 'DESC')
                    ->orderBy('created_at', 'DESC')
                    ->get(['posts.*', 'groups.name AS group_name']);
    }

    /**
     * Store a newly created post in storage.
     *
     * @param  App\Http\Requests\PostsRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(PostsRequest $request)
    {
        $validated = $request->validated();

        $group = Groups::find($validated->group_id);

        if(is_null($group)) {
            return response()->json('Group id not found, please enter a valid group_id!', 404);
        }

        $post = new Posts;
        $post->content      = $request->content;
        $post->group_id     = $request->group_id;
        $post->image_url    = $request->image_url;
        $post->is_fixed     = $request->is_fixed;
        $post->save();

        return $post;
    }

    /**
     * Display the specified post.
     *
     * @param  \App\Models\Posts  $post
     * @return \Illuminate\Http\Response
     */
    public function show(Posts $post)
    {
        return $post;
    }

    /**
     * Update the specified post in storage.
     *
     * @param  App\Http\Requests\PostsRequest  $request
     * @param  \App\Models\Posts  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PostsRequest $request, $id)
    {
        $validated = $request->validated();

        $post = Posts::find($id);

        $post->content      = $request->content;
        $post->group_id     = $request->group_id;
        $post->image_url    = $request->image_url;
        $post->is_fixed     = $request->is_fixed;
        
        $post->update();

        return $post;
    }

    /**
     * Remove the specified post from storage.
     *
     * @param  \App\Models\Posts  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = Posts::find($id);

        if(is_null($post)) {
            return response()->json('The operation could not be performed successfully.', 404);
        }

        $post ->delete();
        return response()->noContent();
    }
}
